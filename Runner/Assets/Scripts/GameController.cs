﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _level = null;
    [SerializeField] private GameObject _player = null;
    //[SerializeField] private GameObject _coints = null;
    [SerializeField] private UI _ui = null;

    private bool _onPlayLevel = false;

    private void Update()
    {
        if (_onPlayLevel)
        {
            _ui.SetCoint(_player.GetComponent<Player>().GetScore());
        }
    }

    private void Start()
    {
        _ui.OnPlay += LoadGame;
        _ui.OnLose += LoseGame;
        _ui.OnRestart += LoadGame;
    }

    private void LoadGame()
    {
        _level.SetActive(true);
        _player.SetActive(true);
        _onPlayLevel = true;
    }

    private void LoseGame()
    {
        _onPlayLevel = false;
        _ui.ShowCoints(_player.GetComponent<Player>().GetCoints());
        _level.SetActive(false);
        _player.SetActive(false);
    }
}
