﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSegment : MonoBehaviour
{
    [SerializeField] private Transform _nextPoint = null;
    [SerializeField] private Transform _parrentTransform = null;

    public Vector3 GetNextpoint => _nextPoint.position;
    public Vector3 GetTransformPosition 
    { 
        get 
        { 
            return _parrentTransform.position; 
        } 
        set 
        { 
            _parrentTransform.position = value; 
        } 
    }
    public System.Action<RoadSegment> OnInvisible;

    private void OnBecameInvisible()
    {
        OnInvisible?.Invoke(this);
    }
}
