﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private float _startPosition;
    private float _offset;

    public float GetOffset=>_offset;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startPosition = Input.mousePosition.x;
        }

        if (Input.GetMouseButton(0))
        {
            _offset = -(_startPosition - Input.mousePosition.x);
            _offset /= Screen.width;
            _startPosition = Input.mousePosition.x;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _offset = 0f;
        }
    }
}
