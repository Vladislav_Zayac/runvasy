﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private GameObject _startScreen = null;
    [SerializeField] private GameObject _gameScreen = null;
    [SerializeField] private GameObject _loseScreen = null;

    public System.Action OnPlay;
    public System.Action OnLose;
    public System.Action OnRestart;

    private void Awake()
    {
        _startScreen.SetActive(true);
    }

    public void SetCoint(float coint)
    {
        _gameScreen.GetComponentInChildren<Text>().text = Convert.ToString((int)coint);
    }

    public void ShowCoints(float coint)
    {
        _loseScreen.GetComponentInChildren<Text>().text = Convert.ToString((int)coint);
    }

    public void StartGame()
    {
        _startScreen.SetActive(false);
        _gameScreen.SetActive(true);
        OnPlay?.Invoke();
    }

    public void RestartGame()
    {
        _loseScreen.SetActive(false);
        _gameScreen.SetActive(true);
        OnRestart?.Invoke();
    }

    public void LoseGame()
    {
        _loseScreen.SetActive(true);
        OnLose?.Invoke();
        _gameScreen.SetActive(false);
    }
}
