﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystems : MonoBehaviour
{
    private int _coints = 0;

    Gamedata gamedata = new Gamedata();

    public void AddCoints(int coints) => _coints += coints;

    public int GetCoints() => _coints;

    private void Start()
    {
        if (PlayerPrefs.HasKey("Gamedata"))
        {
            gamedata = JsonUtility.FromJson<Gamedata>(PlayerPrefs.GetString("Gamedata"));
        }
    }

    private void OnApplicationQuit()
    {
        gamedata.Coins = _coints;
        string json = JsonUtility.ToJson(gamedata);
        PlayerPrefs.SetString("Gamedata", json);
    }
}
