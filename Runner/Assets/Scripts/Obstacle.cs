﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private GameObject _parent = null;

    public System.Action OnInvisible;

    private void OnBecameInvisible()
    {
        OnInvisible?.Invoke();
        Destroy(_parent);
    }
}
