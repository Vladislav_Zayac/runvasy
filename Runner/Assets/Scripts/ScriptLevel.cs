﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptLevel : MonoBehaviour
{
    [SerializeField] private GameObject _roadPrefab = null;
    [SerializeField] private GameObject _obstaclePrefab = null;
    [SerializeField] private GameObject _cointPrefab = null;
    [SerializeField] private Transform _startPoint = null;
    [SerializeField] private int _startSegmentCount = 10;
    [SerializeField] private int _obstacleInStringCount = 4;
    [SerializeField] private float _obstaclesBetweenNumber = 2f;
    [SerializeField] private float _cleanField = 10f;
    [SerializeField] private int _complexityGame = 5;
    [SerializeField] private int _spawnCoin = 1;

    private Vector3 _lastPosition;
    private float currentLength;
    private float betweenPoints;
    private float currentPoint;
    private float weidthSegment;
    private System.Random rnd = new System.Random();
    private bool OnReplace = false;
    private bool EmptyLine = false;
    //private int _startSeed = 2;

    private void OnDisable()//Дописать сброс уровня
    {
        ClearParent();
        _lastPosition = new Vector3();
        currentLength = 0;
        betweenPoints = 0;
        currentPoint = 0;
        weidthSegment = 0;
        OnReplace = false;
    }

    private void ClearParent()
    {
        foreach (Transform child in transform)
        {
            if (child.name != "StartPoint")
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

    public void GenerateRoad()
    {
        Vector3 generationPosition = _startPoint.position;
        for(int i = 0; i < _startSegmentCount; i++)
        {
            GameObject road = Instantiate(_roadPrefab);
            road.transform.position = generationPosition;
            road.transform.SetParent(transform);

            generationPosition = road.GetComponentInChildren<RoadSegment>().GetNextpoint;
            road.GetComponentInChildren<RoadSegment>().OnInvisible += SegmentReplaces;
            if (i == _startSegmentCount - 1)
                _lastPosition = generationPosition;
        }
    }
    
    private void OnEnable()
    {
        GenerateRoad();
        GenerateObstacle();
    }

    private void SegmentReplaces(RoadSegment roadSegment)
    {
        roadSegment.GetTransformPosition = _lastPosition;
        _lastPosition = roadSegment.GetNextpoint;
    }

    private void GenerateObstacle()
    {
        float roadLength = Vector3.Distance(_startPoint.position, _lastPosition);
        currentLength = _cleanField;
        weidthSegment = _roadPrefab.transform.lossyScale.x;
        betweenPoints = (weidthSegment - 1f) / 3;
        currentPoint = -(weidthSegment / 2) + 0.5f;
        while (currentLength < roadLength)
        {
            SpawnLineObstacle();
        }
    }

    private void ObstacleReplaces()
    {
        OnReplace = true;
    }

    private void LateUpdate()
    {
        if (OnReplace)
        {
            SpawnLineObstacle();
            if (EmptyLine)
            {
                SpawnLineObstacle();
                EmptyLine = false;
            }
            OnReplace = false;
        }
    }

    private void SpawnLineObstacle()
    {
        int counter = 0;
        bool createAction = false;

        for (int i = 0; i < _obstacleInStringCount; i++)
        {
            //if (!createAction)
            //{
            //    GameObject emptyObject = Instantiate(new GameObject());
            //    emptyObject.transform.position = new Vector3(0, 0.5f, currentLength);
            //    emptyObject.transform.SetParent(transform);
            //    createAction = true;
            //}
            if (counter < _obstacleInStringCount - 1)
            {
                if (rnd.Next(0, 10) > _complexityGame)
                {
                    Vector3 localPosition = new Vector3(currentPoint + (betweenPoints * i), 0, currentLength);
                    GameObject obstacle = Instantiate(_obstaclePrefab);
                    obstacle.transform.position = localPosition;
                    obstacle.transform.SetParent(transform);
                    if (!createAction)
                    {
                        obstacle.GetComponentInChildren<Obstacle>().OnInvisible += ObstacleReplaces;
                        createAction = true;
                    }
                    counter++;
                }
                else if (rnd.Next(0, 50) == _spawnCoin)
                {
                    Vector3 localPosition = new Vector3(currentPoint + (betweenPoints * i), 0, currentLength);
                    GameObject coint = Instantiate(_cointPrefab);
                    coint.transform.position = localPosition;
                    coint.transform.SetParent(transform);
                    if (!createAction)
                    {
                        coint.GetComponentInChildren<Coints>().OnInvisible += ObstacleReplaces;
                        createAction = true;
                    }
                }
                else
                {
                    EmptyLine = true;
                }
            }
        }

        currentLength += _obstaclesBetweenNumber;
    }
}
