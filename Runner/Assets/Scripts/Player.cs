﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private float _speed = 0f;
    [SerializeField] private InputHandler _inputHandler = null;
    [SerializeField] private float _roadWidth = 0f;
    [SerializeField] public GameObject _camera = null;
    [SerializeField] private AnimationController _animation = null;
    [SerializeField] private UI _ui = null;
    [SerializeField] private GameObject _childPlayer = null;

    private float _nowSpeed;

    [NonSerialized]public bool _onMove;

    /// <summary>
    /// Переменная, хранящая количество найденных монет за забег
    /// </summary>
    private int _score = 0;

    /// <summary>
    /// Переменная, хранящая общее количество собранных монет
    /// </summary>
    private int _coints;

    private Vector3 _startCamera;

    private Vector3 _startPlayer;

    Gamedata gamedata = new Gamedata();

    public float GetCoints() => _coints;
    public float GetScore() => _score;

    private void Awake()
    {
        _startPlayer = transform.position;
    }

    private void Start()
    {
        if (PlayerPrefs.HasKey("Gamedata"))
        {
            gamedata = JsonUtility.FromJson<Gamedata>(PlayerPrefs.GetString("Gamedata"));
        }
        _startCamera = new Vector3(0, 0, 0);
    }

    private void OnEnable()
    {
        _camera.transform.position = _startCamera;
        transform.position = _startPlayer;
        _nowSpeed = _speed;
        ResetScore();
        _animation.SetTrigger("IsRun");
        _onMove = true;
        _childPlayer.transform.position = new Vector3(0, 0.53f, 0);
    }

    private void ResetScore()
    {
        _score = 0;
    }

    private void Update()
    {
        Debug.Log(_speed);
        if (_onMove)
            Move();
    }

    private void Move()
    {
        Vector3 horisontalOffset = _inputHandler.GetOffset * _roadWidth * Vector3.right;
        Vector3 forwardOffset = _nowSpeed * Time.deltaTime * Vector3.forward;
        if(Mathf.Abs(transform.position.x + horisontalOffset.x) > _roadWidth / 2)
            transform.Translate(forwardOffset);
        else
            transform.Translate(horisontalOffset+forwardOffset);
        _camera.transform.Translate(forwardOffset);
    }

    private void Die()
    {
        _onMove = false;
        _camera.transform.Translate(Vector3.back);
        _coints += _score;
        Debug.Log("Lose!");
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Barrier")
        {
            StartCoroutine(Dead());
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Coin")
        {
            StartCoroutine(FindCoin());
            _speed += 0.4f;
        }
    }

    private IEnumerator Dead()
    {
        Die();
        _animation.SetTrigger("IsDead");
        yield return new WaitForSeconds(3);
        _ui.LoseGame();
    }

    private IEnumerator FindCoin()
    {
        _score += 1;
        yield return null;
    }

    private void OnApplicationQuit()
    {
        gamedata.Coins = _coints;
        string json = JsonUtility.ToJson(gamedata);
        PlayerPrefs.SetString("Gamedata", json);
    }
}

[System.Serializable]
public class Gamedata
{
    public int Coins;

    public Gamedata()
    {
        Coins = 0;
    }
}
