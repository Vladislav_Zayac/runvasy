﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshDeformtor : MonoBehaviour
{
    [SerializeField] private float _speed = 0f;
    [SerializeField] private float _radius = 0f;
    [SerializeField] private float offsetLength = 0f;
    [SerializeField] private MeshFilter _meshFilter = null;

    private Vector3 _direction;
    private Vector3 _startMousePosition;
    private float _sensitivity = 25f;


    private void Update()
    {
        InputHandler();
        Movement();
        Deformation();
    }

    private void Movement()
    {
        if(_direction == Vector3.zero)
        {
            return;
        }
        else
        {
            transform.Translate(_speed * Time.deltaTime * _direction);
        }
    }

    private void InputHandler()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _startMousePosition = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            if(Vector3.Distance(Input.mousePosition, _startMousePosition) > _sensitivity)
            {
                Vector3 screenDirection = (Input.mousePosition - _startMousePosition).normalized;
                _direction.x = screenDirection.x;
                _direction.z = screenDirection.y;
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            _direction = Vector3.zero;
        }
    }

    private Vector3[] vertices;

    private void Deformation()
    {
        vertices = _meshFilter.mesh.vertices;
        Vector3 localPosition = _meshFilter.transform.InverseTransformPoint(transform.position);
        Vector3 localPositionXZ = localPosition;
        localPositionXZ.y = 0f;

        for (int i = 0; i < vertices.Length; i++)
        {
            if(Vector3.Distance(vertices[i], localPositionXZ) < _radius)
            {
                float offsetMultiplier = 1f - (Vector3.Distance(vertices[i], localPositionXZ) / _radius);
                vertices[i] += Vector3.down * offsetLength * offsetMultiplier;
            }
        }

        _meshFilter.mesh.SetVertices(vertices);
        _meshFilter.mesh.RecalculateNormals();
    }
}
