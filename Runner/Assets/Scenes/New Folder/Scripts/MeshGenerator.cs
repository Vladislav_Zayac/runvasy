﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class MeshGenerator : MonoBehaviour
{
    [SerializeField] private float _width = 50f;
    [SerializeField] private float _distanceBeetwenPoints = 5;
    [SerializeField] private Material _material = null;

    private MeshFilter _meshFilter = null;
    private MeshRenderer _meshRenderer = null;

    private void Awake()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshRenderer = GetComponent<MeshRenderer>();
    }
    private void Start()
    {
        Generate();
    }

    private void Generate()
    {
        List<Vector3> vertices = new List<Vector3>();
        Vector3 startPoint = new Vector3(-_width / 2, 0, -_width / 2);
        int pointCount = Mathf.RoundToInt(_width / _distanceBeetwenPoints) + 1;

        for (int i = 0; i < pointCount; i++)
        {
            startPoint.z = -_width / 2f + _distanceBeetwenPoints * i;

            for (int j = 0; j < pointCount; j++)
            {
                startPoint.x = -_width / 2f + _distanceBeetwenPoints * j;
                vertices.Add(startPoint);

                //GameObject point = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                //point.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                //point.transform.SetParent(transform);
                //point.transform.localPosition = startPoint;
            }
        }

        List<int> triangles = new List<int>();

        for (int i = 0; i < vertices.Count - pointCount; i++)
        {
            if ((i + 1) % pointCount == 0)
            {
                continue;
            }
                triangles.Add(i);
                triangles.Add(i+pointCount);
                triangles.Add(i+pointCount+1);
                triangles.Add(i);
                triangles.Add(i+pointCount+1);
                triangles.Add(i+1);
        }

        Mesh mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.RecalculateNormals();

        _meshFilter.mesh = mesh;
        _meshRenderer.material = _material;
    }
}
